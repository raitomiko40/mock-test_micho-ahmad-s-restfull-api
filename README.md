# Link Website

https://mock-test-micho-ahmad-s-client-side.vercel.app/

id Login yang bisa digunakan :

- 1234
- 1122

anda juga bisa melakukan register

# link gitlab

- client-side : https://gitlab.com/raitomiko40/mock-test_micho-ahmad-s-client-side
- Servere-side : https://gitlab.com/raitomiko40/mock-test_micho-ahmad-s-restfull-api

# resFull APi

- https://odd-lime-meerkat-gear.cyclic.app/


# Jawaban

1. Apakah kegunaan JSON pada REST API?

- JSON berguna sebagai wadah data/informasi pada saat REST API melakukan pertukaran data. JSON memiliki keunggulan yang dapat dibaca berbagai macam bahasa pemrograman.

2. Jelaskan bagaimana REST API bekerja?

- REST API bekerja dengan cara memanipulasi data dan saling dipertukarkan antara aplikasi dan server melalui protokol komunikasi, yang biasanya digunakan adalah HTTP.

# Deksripsi

Haloo, Saya Micho Ahmad S dari kelas Full-Stack Web Wave 31, facilitator saya Alimuddin Hasan.
Pada web yang saya buat ini saya menerapkan fitur Login dan Register untuk sistem authentication dan authorization .

Web ini memiliki skema dengan gambaran user yang dapat menghandel/manipulasi data dari Task/Todo seperti membuat task , mengedit task, dan mengapus task .


Saya juga sudah menerapkan redux untuk menghandel beberapa fitur seperti component Todolist dan userdata.

Untuk data yang ada baik data user dan data Todo telah terintergrasi dengan API dan DBMS dengan menggunakan postgreSQL.


Terimakasih untuk tim Binar Job connect, Terbaikk👍
